import Vue from "vue";
import Router from "vue-router";
import { FeedRoutes } from "@/modules/Feed";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [...FeedRoutes]
});
