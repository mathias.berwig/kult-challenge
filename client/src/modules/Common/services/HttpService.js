// We're using axios here because it should perform better than native fetch
// https://vuejs.org/v2/cookbook/using-axios-to-consume-apis.html#Fetch-API
import axios from "axios";

const API_ENDPOINT_URL = process.env.VUE_APP_API_ENDPOINT_URL;

/**
 * Main HTTP communication service and container for HTTP clients.
 * We'll use this as base service for every network request on the app, making it
 * easily replaceable/maintenable.
 */
class HttpService {
  constructor() {
    console.log({ API_ENDPOINT_URL });
    this.client = axios.create({
      baseURL: API_ENDPOINT_URL
    });
  }

  /**
   * At this moment, this is a simple wrapper for axios. In the future, we could
   * add authorization middleware/handlers or anything needed here.
   */
  async request(url, options) {
    const response = await this.client.request(url, options);
    // Placeholder for dealing with API errors
    // if (response.status >= 200 && response.status < 300)
    return response.data;
  }
}

const instance = new HttpService();

Object.freeze(instance);

export default instance;
