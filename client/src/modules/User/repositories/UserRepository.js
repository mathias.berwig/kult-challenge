import { HttpService } from "@/modules/Common";

export default {
  ensureProfile: async userId => {
    return HttpService.request(`/user/${userId}`);
  }
};
