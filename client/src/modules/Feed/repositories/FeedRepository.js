import { HttpService } from "@/modules/Common";

export default {
  ensureFeed: async () => {
    return HttpService.request("/feed");
  },
  ensureUserFeed: async userId => {
    return HttpService.request(`/feed/${userId}`);
  },
  ensureContent: async contentId => {
    return HttpService.request(`/content/${contentId}`);
  }
};
