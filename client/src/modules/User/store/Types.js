export default {
  Actions: {
    loadProfile: "loadProfile"
  },
  Mutations: {
    setProfile: "setProfile"
  },
  Getters: {
    getProfile: "getProfile"
  }
};
