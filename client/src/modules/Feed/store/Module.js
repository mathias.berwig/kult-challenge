import { StatusConst } from "@/modules/Common";
import FeedRepository from "../repositories/FeedRepository";
import Types from "./Types";

const initialFeedState = {
  status: StatusConst.IDLE,
  data: []
};

export default {
  state: () => ({
    feed: { ...initialFeedState },
    feedByUser: {},
    contents: {},
    contentStatus: StatusConst.IDLE
  }),
  mutations: {
    // Feed
    [Types.Mutations.setFeed]: (state, { status, feedItems = [] }) => {
      state.feed.status = status;
      if (status === StatusConst.SUCCESS) {
        state.feed.data = [];
        feedItems.forEach(feedItem => {
          state.contents[feedItem._id] = feedItem;
          state.feed.data.push(feedItem._id);
        });
      }
    },
    // User Feed
    [Types.Mutations.setUserFeed]: (
      state,
      { userId, status, feedItems = [] }
    ) => {
      if (!state.feedByUser[userId]) {
        state.feedByUser[userId] = { ...initialFeedState };
      }
      state.feedByUser[userId].status = status;
      if (status === StatusConst.SUCCESS) {
        state.feedByUser[userId].data = [];
        feedItems.forEach(feedItem => {
          state.contents[feedItem._id] = feedItem;
          state.feedByUser[userId].data.push(feedItem);
        });
      }
    },
    // Content
    [Types.Mutations.setContent]: (state, { status, contentId, content }) => {
      state.contentStatus = status;
      if (status === StatusConst.SUCCESS) {
        state.contents[contentId] = content;
      }
    }
  },
  actions: {
    // Feed
    [Types.Actions.loadFeed]: async ({ commit }) => {
      commit(Types.Mutations.setFeed, { status: StatusConst.LOADING });
      try {
        const feedItems = await FeedRepository.ensureFeed();
        commit(Types.Mutations.setFeed, {
          status: StatusConst.SUCCESS,
          feedItems
        });
      } catch (error) {
        commit(Types.Mutations.setFeed, { status: StatusConst.ERROR });
      }
    },
    // User Feed
    [Types.Actions.loadUserFeed]: async ({ commit }, { userId }) => {
      commit(Types.Mutations.setUserFeed, {
        userId,
        status: StatusConst.LOADING
      });
      try {
        const userFeed = await FeedRepository.ensureUserFeed(userId);
        commit(Types.Mutations.setUserFeed, {
          userId,
          status: StatusConst.SUCCESS,
          userFeed
        });
      } catch (error) {
        commit(Types.Mutations.setUserFeed, {
          userId,
          status: StatusConst.ERROR
        });
      }
    },
    // Content
    [Types.Actions.loadContent]: async ({ commit }, { contentId }) => {
      commit(Types.Mutations.setContent, { status: StatusConst.LOADING });
      try {
        const content = await FeedRepository.ensureContent(contentId);
        commit(Types.Mutations.setContent, {
          contentId,
          status: StatusConst.SUCCESS,
          content
        });
      } catch (error) {
        commit(Types.Mutations.setContent, { status: StatusConst.ERROR });
      }
    }
  },
  getters: {
    // Feed
    [Types.Getters.getFeedStatus]: state => state.feed.status,
    [Types.Getters.getFeedItems]: state =>
      state.feed.data.map(contentId => state.contents[contentId]),
    // User Feed
    [Types.Getters.getUserFeedItems]: state => userId =>
      (state.feedByUser[userId] || initialFeedState).data.map(
        contentId => state.contents[contentId]
      ),
    // Content
    [Types.Getters.getContentStatus]: state => state.contentStatus,
    [Types.Getters.getContent]: state => contentId => state.contents[contentId]
  }
};
