'use strict';

const UserModel = require('../models/user_schema');

const UserController = {
  create: async (req, res) => {
    const newUser = await UserModel.create(req.body);
    res.status(201).json(newUser);
  },
  get: async (req, res) => {
    const user = await UserModel.findById(req.params.id);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send('User not available');
    }
  },
  list: async (req, res) => {
    const allUsers = await UserModel.find();
    res.status(200).json(allUsers);
  },
  update: async (req, res) => {
    const user = await UserModel.findByIdAndUpdate(req.params.id, req.body, {
      useFindAndModify: false,
      new: true,
    });
    res.status(201).json(user);
  },
  remove: async (req, res) => {
    const user = await UserModel.findByIdAndDelete(req.params.id);
    if (user) {
      res.status(200).json(user);
    } else {
      res.status(404).send('User not available');
    }
  },
}

module.exports = UserController;
