const { Schema, model } = require('mongoose');

const contentSchema = new Schema(
  {
    name: {
      type: String,
      required: [true, 'name field is required'],
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'users',
      required: [true, 'owner field is required'],
    },
    src: {
      type: String,
      required: [true, 'src field is required'],
    },
    thumbnail: {
      type: String,
      required: [true, 'src field is required'],
    },
  },
  { timestamps: true },
);

module.exports = model('content', contentSchema);
