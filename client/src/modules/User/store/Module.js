import { StatusConst } from "@/modules/Common";
import UserRepository from "../repositories/UserRepository";
import Types from "./Types";

const initialProfileState = {
  status: StatusConst.IDLE,
  data: []
};

export default {
  state: () => ({
    profiles: {}
  }),
  mutations: {
    [Types.Mutations.setProfile]: (state, { userId, status, data }) => {
      if (!state.profiles[userId]) {
        state.profiles[userId] = { ...initialProfileState };
      }
      state.profiles[userId].status = status;
      if (status === StatusConst.SUCCESS) {
        state.profiles[userId].data = data;
      }
    }
  },
  actions: {
    [Types.Actions.loadProfile]: async ({ commit }, { userId }) => {
      commit(Types.Mutations.setProfile, {
        userId,
        status: StatusConst.LOADING
      });
      try {
        const profile = await UserRepository.ensureProfile(userId);
        commit(Types.Mutations.setProfile, {
          userId,
          status: StatusConst.SUCCESS,
          data: profile
        });
      } catch (error) {
        commit(Types.Mutations.setProfile, {
          userId,
          status: StatusConst.ERROR
        });
      }
    }
  },
  getters: {
    [Types.Getters.getProfile]: state => userId =>
      state.profiles[userId] || initialProfileState
  }
};
