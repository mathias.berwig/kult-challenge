/**
 * This is a fork ok "Vue.js Web Monetization" on GitHub
 * https://github.com/tarotaro080808/vue-web-monetization/blob/master/src/vue-web-monetization.js
 */

export default {
  install(Vue) {
    Vue.prototype.$monetization = Vue.observable({
      start: {
        state: document.monetization && document.monetization.state,
        paymentPointer: null,
        requestId: null
      },
      progress: {
        assetCode: null,
        assetScale: null,
        totalAmount: 0,
        formattedTotalAmount: 0
      }
    });

    if (!document.monetization) {
      return;
    }

    document.monetization.addEventListener("monetizationstart", function(ev) {
      const { paymentPointer, requestId } = ev.detail;
      Vue.prototype.$monetization.start = Object.assign(
        {},
        {
          state: document.monetization.state,
          paymentPointer,
          requestId
        }
      );
    });

    document.monetization.addEventListener("monetizationprogress", function(
      ev
    ) {
      const { amount, assetCode, assetScale = 1 } = ev.detail;
      let totalAmount = Vue.prototype.$monetization.progress.totalAmount;
      totalAmount += Number(amount);
      const formattedTotalAmount = (
        totalAmount * Math.pow(10, -assetScale)
      ).toFixed(assetScale);

      Vue.prototype.$monetization.progress = Object.assign(
        {},
        {
          assetCode,
          assetScale,
          totalAmount,
          formattedTotalAmount
        }
      );
    });

    document.monetization.addEventListener("monetizationstop", function(ev) {
      const { paymentPointer, requestId } = ev.detail;
      Vue.prototype.$monetization.start = Object.assign(
        {},
        {
          state: document.monetization.state,
          paymentPointer,
          requestId
        }
      );
    });
  }
};
