export default {
  Actions: {
    loadFeed: "loadFeed",
    loadUserFeed: "loadUserFeed",
    loadContent: "loadContent"
  },
  Mutations: {
    setFeed: "setFeed",
    setUserFeed: "setUserFeed",
    setContent: "setContent"
  },
  Getters: {
    // Feed
    getFeedStatus: "getFeedStatus",
    getFeedItems: "getFeedItems",
    // User Feed
    getUserFeedItems: "getUserFeedItems",
    // Content
    getContentStatus: "getContentStatus",
    getContent: "getContent"
  }
};
