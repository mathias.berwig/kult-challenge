export { default as FeedRepository } from "./repositories/FeedRepository";
export { default as FeedModule } from "./store/Module";
export { default as FeedTypes } from "./store/Types";

export const FeedRoutes = [
  {
    path: "/",
    name: "feed",
    component: () => import("./views/Feed.vue")
  },
  {
    path: "/content/:id",
    name: "content-details",
    component: () => import("./views/Content.vue")
  }
];
