import Vue from "vue";
import Vuex from "vuex";
import { FeedModule as feed } from "@/modules/Feed";
import { UserModule as user } from "@/modules/User";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    feed,
    user
  }
});
