const express = require('express');

const {
  create: createUser,
  list: listUsers,
  get: getUser,
  update: updateUser,
  remove: removeUser,
} = require('../controllers/user_controller');

const {
  create: createContent,
  remove: removeContent,
  get: getContent,
  list: listContent,
  listByUser: listContentByUser,
} = require('../controllers/content_controller');

const router = express.Router();

router
  // User
  .post('/user', createUser)
  .get('/users', listUsers)
  .get('/user/:id', getUser)
  .put('/user/:id', updateUser)
  .delete('/user/:id', removeUser)
  // Content
  .post('/content', createContent)
  .get('/content/:id', getContent)
  .get('/feed', listContent)
  .get('/feed/:userId', listContentByUser)
  .delete('/content/:id', removeContent);

module.exports = router;
