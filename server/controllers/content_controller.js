'use strict';

const ContentModel = require('../models/content_schema');

const ContentController = {
  create: async (req, res) => {
    const newContent = await ContentModel.create(req.body);
    res.status(201).json(newContent);
  },
  get: async (req, res) => {
    const content = await ContentModel.findById(req.params.id).populate('owner');
    if (content) {
      res.status(200).json(content);
    } else {
      res.status(404).send('Content not available');
    }
  },
  list: async (req, res) => {
    const allContent = await ContentModel.find().populate('owner');
    res.status(200).json(allContent);
  },
  listByUser: async (req, res) => {
    const userContent = await ContentModel.find({ owner: req.params.userId });
    res.status(200).json(userContent);
  },
  remove: async (req, res) => {
    const content = await ContentModel.findByIdAndDelete(req.params.id);
    if (content) {
      res.status(200).json(content);
    } else {
      res.status(404).send('Content not available');
    }
  },
};

module.exports = ContentController;
