# kult-challenge

## The project
This is the output of my studies with Vue.JS and Web Monetization. The project is a MEVN (Mongo, Express, Vue and Node) stack and allows the user to navigate into a feed of content (images), click on one of them and see it in details while accepting micropayments. While an image is opened in detail, it will show, at the footer of the page the monetization status. While the project itself is pretty simple, it has the purpose of demonstrating the skills acquired in the short period it was developed.

## Run instructions
### For API
Everything needed is on `.env` (the database connection URL). Database has all the data necessary for the app to work.

1. `cd server && npm install`
2. `npm run serve`
3. It should show `Listening On http://localhost:9000/api` and `connected to database`.
Database as now (2021/05/01) is fully visible to anyone on the internet.

#### Postman Docs
If needed, one can do CRUD operations in the database by referring to the Postman Collection on the `/docs`, which has pre-filled arguments and queries for demonstrating how the system works.

### For Vue web client
No configuration needed for demo purposes. `.env` has the API URL and the Web Monetization wallet address.
1. `cd client && npm install`
2. `npm run serve`
3. It should show `App running at: Local: http://localhost:8080/`
